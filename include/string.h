/* string.h, written by Safal Aryal for the sLIBC (Safal C Library) */

#ifndef STRING_H
#define STRING_H

#define NULL 0

#include <stddef.h>

/* The main functions of string.h */
char *strcpy(char *to, const char *from);
char *strncpy(char *to, const char *from, size_t n);
char *strcat(char *to, const char *from);
char *strncat(char *to, const char *from, size_t n);
size_t strxfrm(char *to, const char *from, size_t n);
size_t strlen(const char *str);
int strcmp(const char *str1, const char *str2);
int strncmp(const char *str1, const char *str2, size_t n);
char *strchr(const char *str, int c);
char *strrchr(const char *str, int c);
size_t strspn(const char *str1, const char *str2);
size_t strcspn(const char *str, const char *str2);
char *strpbrk(const char *str1, const char *str2);
char *strstr(const char *haystack, const char *needle);
char *strtok(char *str, const char *delim);
char *strerror(int errno);
void *memset(void *str, int c, size_t n);
void *memcpy(void *str1, const void *str2, size_t n);
void *memmove(void *str1, const void *str2, size_t n);
int memcmp(const void *str1, const void *str2, size_t n);
void *memchr(const void *str, int c, size_t n);

/* The numeric conversion functions of string.h */
float atof(const char *str);
int atoi(const char *str);
int atoll(const char *str);
float strtof(const char *str);
float strtod(const char *str);
float strtold(const char *str);
long int strtol(const char *str);
long int strtoll(const char *str);
size_t strtoul(const char *str);
size_t strtoull(const char *str);

#endif
