#ifndef STDLIB_H
#define STDLIB_H

typedef struct {
    int quot;
    int rem;
} div_t;

typedef struct {
    long int quot;
    long int rem;
} ldiv_t;

int abs(int x);
long int labs(long int x);
div_t div(int num, int denom);
int system(const char *command);
ldiv_t ldiv(long int num, long int denom);

#endif
