#ifndef STDIO_H
#define STDIO_H

#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

extern int putchar(int c);
extern int puts(char *string);

#endif
