#include <string.h>

/* String and memory manipulation functions */

size_t strlen(const char *str) 
{
    size_t len;

    while(str[len]) {
        len++;
    }

    return len;
}

size_t strspn(const char *str1, const char *str2)
{
    size_t i;
    const char *p;
    for(i=0; *str1; str1++, i++) {
        for(p=str2; *p && *p != *str1; p++)
            ;
        if(!*p)
            break;
    }
    return i;
}

size_t strcspn(const char *str1, const char *str2)
{

    size_t i;
    const char *p;
    for(i=0; *str1; str1++, i++) {
        for(p=str2; *p && *p != *str1; p++)
            ;
        if(*p)
            break;
    }
    return i;
}

char *strcpy(char *to, const char *from)
{
    int i;
    int len = strlen(from);

    for(i=0;i<=len;i++) {
        to[i] = from[i];
    }

    return to;
}

char *strncpy(char *to, const char *from, size_t n)
{
    int i;

    for(i=0;i<=n;i++) {
        to[i] = from[i];
    }

    return to;
}

char *strcat(char *str1, const char *str2)
{
    int i,j;
    int len1 = strlen(str1);
    int tlen = len1 + strlen(str2);

	for(i=0,j=len1;i<=tlen,j<=tlen;i++,j++) {
		str1[j] = str2[i];
	}

	return str1;
}

/* Thanks for this function fengl.org */
char *strtok(char *str, const char *delim)
{
    static char *buffer;
    if(str!=NULL) buffer = str;

    char *ret = buffer, *b;
    const char *d;

    for(b=buffer;*b!='\0';b++) {
        for(d=delim; *d!='\0';d++) {
            if(*b==*d) {
                *b = '\0';
                buffer = b+1;

                if(b==ret) {
                    ret++;
                    continue;
                }
            }
        }
    }
    
    return ret;
}

char *strncat(char *str1, const char *str2, size_t n) 
{
	int i,j;
	int len1 = strlen(str1);
	int tlen = n;

	for(i=0,j=len1;i<=tlen,j<=tlen;i++,j++) {
		str1[j] = str2[i];
	}

	return str1;
}

int strcmp(const char *str1, const char *str2)
{
    int i = 0;
    while(*str1!='\0' && *str2!='\0') {
        if(*str1!=*str2) {
            i++;
        }
    }
    return i;
}

int strncmp(const char *str1, const char *str2, size_t n)
{
    int j;
    int i = 0;
    while(*str1!='\0' && *str2!='\0' && j<=n) {
        if(*str1!=*str2) {
            i++;
        }
        j++;
    }
    return i;
}

char *strchr(const char *str, int c)
{
    while(*str!=(char)c) {
        if(!*str) {
            return NULL;
        }
        *str++;
    }
    return (char *) str;
}

char *strrchr(const char *str, int c)
{
    int i;
    int len = strlen(str);
    while(i<len) {
        if(!*str) {
            return NULL;
        } else if(*str==(char)c) {
            break;
            return (char *) str;
        }
        i++;
    }
}

char *strstr(const char *haystack, const char *needle)
{
    if(!*needle) return haystack;
    char *p1 = (char *) haystack;
    while(*p1) {
        char *p1Begin = p1, *p2 = (char *) needle;
        while(*p1 && *p2 && *p1 == *p2) {
            p1++;
            p2++;
        }
        if(!*p2) {
            return p1Begin;
        }
        p1 = p1Begin + 1;
    }
    return NULL;
}

char *strpbrk(const char *str1, const char *str2) 
{
    while(*str1) {
        if(*str1==str2[0]) {
            return str2[0];
        }
        *str1++;
    }

    return NULL;
}

void *memset(void *str, int c, size_t n)
{
	int i;
	int len;

	char c_m = (char) c;
	char *str_m = (char *) str;
	len = strlen(str); i = len;

	while(i!=0) {
		str_m[i] = c_m;
		i--;
	}

	return str_m;	
}

int memcmp(const void *str1, const void *str2, size_t n)
{
	int i;
	int j;

	const char *str1_m = (const char *) str1;
	const char *str2_m = (const char *) str2;

	while(*str1_m != '\0' && *str2_m!= '\0' && j<=n) {
		if(*str1_m!=*str2_m) {
			i++;
		}
		j++;
	}

	return i;
}

void *memcpy(void *str1, const void *str2, size_t n)
{
	int i;

	char *str1_m = (char *) str1;
	const char *str2_m = (const char *) str2;

	for(i=0;i<=n;i++) {
		str1_m[i] = str2_m[i];
	}

	return str1_m;
}

void *memmove(void *str1, const void *str2, size_t n)
{
	int i;

	char *str1_m = (char *) str1;
	const char *str2_m = (const char *) str2;

	for(i=0;i<=n;i++) {
		str1_m[i] = str2_m[i];
	}

	return str1_m;
}

void *memchr(const void *str, int c, size_t n)
{
	const char *str_m = (char *) str;
	int i;
	while(*str_m!=(char)c && i<=n) {
		if(!*str_m) {
			return NULL;
		}
		i++;
	}
	return (char *) str;
}

/* Numerical conversions */

int atoi(const char *str)
{
    int n = 0;
    int i = 0;
    int sign = 1;
    while(str[i]) {
        if(str[i]==' ') continue;
        else if(str[i]=='-') sign=-1;
        n *= 10;
        n += str[i] - '0';
        i++;
    }
    return n;
}

int atoll(const char *str)
{
    int n = 0;
    int i = 0;
    int sign = 1;
    while(str[i]) {
        if(str[i]==' ') continue;
        else if(str[i]=='-') sign=-1;
        n *= 10;
        n += str[i] - '0';
        i++;
    }
    return n;
}

long int strtol(const char *str)
{
    int n = 0;
    int i = 0;
    int sign = 1;
    while(str[i]) {
        if(str[i]==' ') continue;
        if(str[i]=='-') sign=-1;
        n *= 10;
        n += str[i] - '0';
        i++;
    }
    return n;
}

long int strtoll(const char *str)
{
    int n = 0;
    int i = 0;
    int sign = 1;
    while(str[i]) {
        if(str[i]==' ') continue;
        if(str[i]=='-') sign=-1;
        n *= 10;
        n += str[i] - '0';
        i++;
    }
    return n;
}

size_t strtoul(const char *str)
{
    int i = 0;
    size_t n = 0;
    int sign = 1;
    while(str[i]) {
        if(str[i]==' ') continue;
        else if(str[i]=='-') break;
        n *= 10;
        n += str[i] - '0';
        i++;
    }

    return n;
}

size_t stroull(const char *str)
{
    int i = 0;
    size_t n = 0;
    int sign = 1;
    while(str[i]) {
        if(str[i]==' ') continue;
        else if(str[i]=='-') break;
        n *= 10;
        n += str[i] - '0';
        i++;
    }

    return n;
}

float atof(const char *str)
{
   float n = 0;
   int i = 0;
   int sign = 1;
   while(str[i]) {
        if(str[i]==' ') continue;
        else if(str[i]=='-') sign=-1;
        else if(str[i]=='.')
        n *= 10;
        n += str[i] - '0';
        i++;
   }
   return n;
}
